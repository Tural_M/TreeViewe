﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.IO;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp11
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            treeView1.BeforeSelect += TreeView1_BeforeSelect;
            treeView1.BeforeExpand += TreeView1_BeforeExpand;

        }

        private void TreeView1_BeforeExpand(object sender, TreeViewCancelEventArgs e)
        {
            e.Node.Nodes.Clear();
            if (Directory.Exists(e.Node.FullPath))
            {
                var dir = Directory.GetDirectories(e.Node.FullPath);
                if (dir.Length != 0)
                {
                    for (int i = 0; i < dir.Length; i++)
                    {
                        TreeNode treeNode = new TreeNode(new DirectoryInfo(dir[i]).Name);
                        Addtreeview(treeNode, dir[i]);
                        e.Node.Nodes.Add(treeNode);
                    }
                }
            }
        }
        private void Addtreeview(TreeNode nod, string path)
        {
            var directories = Directory.GetDirectories(path);
            foreach (var item in directories)
            {
                TreeNode treeNode = new TreeNode();
                treeNode.Text = item.Remove(0, item.LastIndexOf("\\") + 1);
                nod.Nodes.Add(treeNode);
            }
        }
        private void TreeView1_BeforeSelect(object sender, TreeViewCancelEventArgs e)
        {
            e.Node.Nodes.Clear();
            if (Directory.Exists(e.Node.FullPath))
            {
                #region
                //var dir = Directory.GetDirectories(e.Node.FullPath);

                //if (dir.Length != 0)
                //{
                //    for (int i = 0; i < dir.Length; i++)
                //    {
                //        TreeNode treeNode = new TreeNode(new DirectoryInfo(dir[i]).Name);

                //        Addtreeview(treeNode, dir[i]);
                //        e.Node.Nodes.Add(treeNode);

                //    }
                //}
                #endregion
                var dir = Directory.GetFileSystemEntries(e.Node.FullPath, "*").Select(dire => new DirectoryInfo(dire))
                              .Where(dirInfo => !dirInfo.Attributes.HasFlag(FileAttributes.Hidden)).ToList();
                if (dir.Count != 0)
                {
                    // e.Node.Nodes[0].Remove();
                    foreach (var t in dir)
                    {
                        var node = e.Node.Nodes.Add(t.Name);
                        FileAttributes attributes = File.GetAttributes(e.Node.FullPath + @"\" + t.Name);
                        if (attributes.HasFlag(FileAttributes.Hidden))
                        {
                            node.Nodes.Add("");
                        }
                    }
                }
                e.Node.Expand();
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            var drivers = DriveInfo.GetDrives();
            foreach (var item in drivers)
            {
                DirectoryInfo directoryInfo = new DirectoryInfo(item.Name);
                if (item.Name == @"D:\")
                    continue;

                var path = directoryInfo.GetDirectories();
                TreeNode treeNode = new TreeNode(item.Name);
                treeNode.Nodes.Add("");
                treeView1.Nodes.Add(treeNode);
            }
        }
    }
}
